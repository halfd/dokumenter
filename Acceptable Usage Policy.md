# Acceptable Usage Policy 1.0.0

* Version 1.0.0: Vedtaget 22. december 2020

## 1. Introduktion

`data.coop` er en forening som har til formål at passe på medlemmernes
data. Foreningen er bygget op omkring følgende fire kerneprincipper:

* Privatlivsbeskyttelse
* Kryptering
* Decentralisering
* Zero-knowledge

Formålet med dette dokument er at tydeliggøre de rettigheder og etiske
retningslinjer, der er forbundet med brugen af `data.coop`'s tjenester og
ressourcer.

### 1.1 Definitioner

|                            |                                                                                                                                                                                                                                                                                                                                                                                                                    |
| -------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| "Foreningen"               | Dækker over foreningen `data.coop`, hvis juridiske virksomhedsform er "forening".                                                                                                                                                                                                                                                                                                               |
| "Kooperativ"               | Dækker over foreningens organiseringsform og ejerskabsforhold, i det `data.coop` gerne vil etablere sig selv som et alternativ til "de store" som f.eks. Google, Microsoft og Facebook, men vil gøre dette kooperativt – altså ejet og drevet i fællesskab, i stedet for finansieret med f.eks. venture kapital – som tilfældet for mange forholdsvis nystartede virksomheder i IT-sektoren er. |
| "Tjenester (ell. ydelser)" | Dækker over de tjenester (ell. ydelser) som `data.coop` tilbyder. En oversigt over disse kan ses på [https://`data.coop`/tjenester/](https://data.coop/tjenester/) .                                                                                                                                                                                                                      |
| "Brugeren"                 | Dækker over den / de juridiske person(er) der benytter foreningens tjenester.                                                                                                                                                                                                                                                                                                                                      |
| "AUP"                      | Acceptable Usage Policy eller *Politik for Acceptabel Brug*. Dækker over dette dokument der fastsætter de vilkår og betingelser for brug af de tjenester stillet til rådighed af foreningen `data.coop`.                                                                                                                                                                                        |

### 1.2 Anvendelsesområde og accept

Denne AUP gør sig gældende – og anses accepteret af brugeren – ved brug
af hjemmesiden `data.coop`, og de dertil knyttede tjenester stillet til
rådighed – evt. tilgået via hjemmesiden
(). AUP’en gælder for samtlige brugere;
besøgende og andre aktører uanset deres eksisterende tilhørsforhold til
`data.coop`.


## 2. AUP – Politik for Acceptabel Brug (Acceptable Usage Policy)

På de følgende sider vil der blive tydeliggjort hvad der hhv. anses som
værende acceptabel brug af `data.coop`, uacceptabel brug af `data.coop`,
samt et afsnit om overtrædelser / sanktioner såfremt brugeren ikke
overholder foreningens AUP.

Det skal dog atter understreges at `data.coop` er en forening og et
kooperativ, med et dertilhørende værdisæt og organisationsform. Det er i
udgangspunktet sådan at foreningens medlemmer stoler mere på hinanden
(materialiseret i foreningen) til at passe på medlemmernes data, end på
andre – oftest kommercielle platforme som f.eks. Facebook og Google.
Relationen imellem `data.coop` og dens brugere – især hvis disse også er
medlemmer og derved også medejere – minder derfor meget lidt om
relationen i et mere klassisk økonomisk forhold imellem en køber og en
sælger – eller i denne kontekst – imellem en køber af en tjenesteydelse
og en profitdrevet platform som sælger denne tjenesteydelse.


### 2.1 Acceptabel brug af `data.coop`

De indholdspolitikker, der er angivet nedenfor, er i høj grad med til at
give vores brugere en positiv oplevelse. For at `data.coop` skal kunne
blive ved med at stille vores tjenester til rådighed for vores brugere,
samt udvikle og forbedre på disse, er vi nødt til at tage visse
forholdsregler mod misbrug, der på sigt kan hindre os i at levere vores
ydelser. Vi beder derfor alle om at overholde politikkerne nedenfor for
at hjælpe os med at opnå dette.

Een af `data.coop`'s mest basale hjørnesten er troen på at man
selv skal have kontrol over ens egen data. Denne grundsætning
danner derfor også den overordnede ramme for *acceptabel brug* af 
`data.coop`'s ydelser.

`data.coop` er en forening og et kooperativ, hvis velbefindende
er tæt knyttet til medlemmernes gøren og laden. I vores brug af
`data.coop`'s ydelser, såvel som i interaktionen med andre medlemmer
(f.eks. på digitale kanaler, til møder, generalforsamlinger o.l.),
forventes det af foreningens brugere, at vi:

**2.1.1** Udviser empati og forståelse for andre mennesker.

**2.1.2** Udviser respekt for andre (ikke-undertrykkende) meninger
og holdninger.

**2.1.3** Er konstruktive og respektfulde i vores kommunikation
vedr. feedback – både det feedback vi måtte have til andre, men
ligeså meget det feedback vi selv ønsker at modtage.

**2.1.4** Er villige til at tage ansvar og – hvis situationen
kræver det – undskylde over for dem, der måtte være blevet berørt af
vores fejl.

**2.1.5** Fokuserer på hvad der er bedst for hele foreningen, og
ikke kun enkeltpersoner. 

**2.1.6** Deltager aktivt – i det omfang vi evner og har lyst til –
i det sociale og faglige miljø i foreningen, evt. via foreningens 
kanal på Freenode: `#data.coop` og/eller på [https://git.data.coop/](https://git.data.coop/).

**2.1.7** De services, som udstilles af `data.coop`, er underlagt foreningens
retningslinjer for acceptabel adfærd og brug af `data.coop`.

**2.1.8** `data.coop` hoster gerne andre frivillige / kooperative / ikke-kommercielle
fællesskabers tjenester, f.eks. en hjemmeside eller løsning for andre fællesskaber.

### 2.2 Uacceptabel brug af `data.coop`

Følgende er en ufuldstændig liste over uønsket adfærd og uacceptabel
brug af `data.coop`.

**2.2.1** Brug af `data.coop`'s tjenester til at dele, poste, lagre, eller
lign., krænkende, pornografisk, o.l. indhold.

**2.2.2** Brug af `data.coop`'s tjenester til at sende / dele spam.

**2.2.3** Brug af `data.coop`'s ressourcer til at udføre Denial of Service angreb.

**2.2.4** Brug af `data.coop`'s ressourcer til at skaffe sig adgang til andre
medlemmers data uden samtykke.

**2.2.5** Igennem `data.coop`'s platform og/eller tjenester at praktisere
trolling, fornærmende eller nedsættende kommentarer og personlige eller
politiske angreb.

**2.2.6** Igennem `data.coop`'s platform at udøve offentlig eller privat chikane.

**2.2.7** Offentliggørelse af andres private oplysninger, såsom en fysisk eller
en e-mail-adresse, uden deres udtrykkelige tilladelse.

**2.2.8** Igennem, og på, `data.coop`'s platform og tjenesteydelser at
promovere synspunkter og holdninger som – med rimelighed – kan tolkes at være
fascistiske, racistiske, sexistiske, homo- og/eller trans- fobiske,
militaristiske, o.l.

## 3. Overtrædelser / sanktioner

Overtrædelse af denne AUP kan medføre sanktioner og, afhængigt af
overtrædelsens alvor og/eller incidens, eksklusion af foreningen.

For nærmere specifikationer omkring hvilke sanktioner kan komme på tale
hvornår, tag venligst fat i et bestyrelsesmedlem.

### 3.1 Eksklusion

`data.coop` forbeholder sig retten til at nægte medlemsskab, og/eller
ekskludere eksisterende medlemmer.

Et afslag på medlemsskab af foreningen kan bl.a. komme på tale, hvis ansøgeren
har kendte forbindelser til / holdninger / synspunkter der grundlæggende strider
imod foreningens kerneprincipper og formål, og / eller klart indikerer at disse
holdninger / synspunkter vil medføre overtrædelser af fællesskabsstandarderne
og/eller på ingen måde ville støtte foreningens formål.

## 4. Ændringer af politik for acceptabel brug

AUP'en kan kun vedtages og / eller ændres ved en generalforsamling.
