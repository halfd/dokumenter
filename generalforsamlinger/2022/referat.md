# Referat generelforsamling 1. juni 2022

Sted: Online net-møde

Vi startede med en navnerunde.

## Deltagere

* Mikkel Munch Mortensen
* Benjamin Balder Bach
* Jesper Hess Nielsen
* Reynir Björnsson
* Víðir Valberg Guðmundsson
* Karsten Højgaard
* Rasmus Malver
* Halfdan Mouritzen
* Thomas Hansen
* Sam Al-Sapti
* Klaus Slott

## Dagsorden

1. Valg af dirigent og referent.
1. Bestyrelsens beretning.
1. Fremlæggelse af regnskab, budget og kontingent.
1. Indkommende forslag.
1. Godkendelse af vedtægtsændringer og Acceptable Use Policy
1. Valg (Jf. § 3).
1. Eventuelt.

## Valg af dirigent og referent.

Vidir er ordstyrer.

Halfdan er referant.

## Bestyrelsens beretning
Vidir fortæller at der ikke er sket det helt store. 
Dog har vi flyttet vores servere fra et datacenter til en bekendts datacenter.
Flytningen blev klaret uden de store kvaler.

Desuden havde vi godt gang i nogle arbejdsdage/møder, som er blevet droppet lidt hen af vejen.

Derudover har vi fået en mastodon instans.

Vi har købt en strømforsyning. Servere har ofte to inputs, der var en strømforsyning der gik af under flytningen.

Vi har brugt penge på en strømforsyning.

Det sidste vi har hørt om strømforsyningen er at blæseren virrer op og ned. Det kan evt. have noget at gøre
med misbrug af Drone instans. Derved større strømforbrug.

Vi burde måske have en sys-admins beretning, netop hvis der er sket events.

Reynir var ved at ryde op i vores Gitea-brugere og opdagede at der var en TensorFlow proces
der brugte al vores CPU.

Det viste sig at være en bruger fra Indonesien, der havde sat et CI job op, der minede crypto.

For nogle få uger siden, skete det så igen. Den samme bruger kom tilbage og lavede præcis samme trick.

Vi har netdata.data.coop hvor man kan se realtime performance på serveren.

Balder og Reynir har været på en Mastodon-workshop. Det ligner at der er rigtig meget vidensdeling,
både teknisk og socialt. Intensionen er at Balder og Reynir vil sætte en virtuel Mastodon-onboarding
op. Der er en masse ting at snakke om.

Det skal nævnes at vi har snakket om at skifte bank, det skyldes at Merkur, hvor vi er nu, har nogle relativt høje gebyrer. Det er ikke sket endnu og Vidir nævner at det nok heller ikke kommer til at ske.
At vi i stedet må vænne os til at der er lidt højere gebyrer.

Der bliver spurgt til om Gitea er åben for alle. Det er den i øjeblikket, men vi har snakket om evt. at
gøre den members only.

Reynir fortæller om at workshoppen tog udgangspunkt i et Mastodon-fork, der hedder Hometown.
En af forskellene er at man kan lave posts der ikke bliver fødereret. Dvs. at det kun er brugere på
samme server der kan læse postet. Det gør det muligt at have et mere lokalt community.
Reynir lægger op til at vi snakker videre på et andet tidspunkt.

Vidir siger at Halfdan og han satte Mastodon op - som udgangspunkt for at køre en hyldevare så at sige.

Decibyte spørger om det er nemt at flytte dataen hvis vi vælger at skifte til Hometown. Det virker som 
om det er nemt at have med at gøre.

Sam spørger ind om det bliver mere besværligt at være på en anden server og også aktiv på social.data.coop
Reynir svarer at det er per post man vælger om man vil føderere eller ej, det skulle ikke indflyde på.
Men tænker også på hvor nemt det er med docker og migrering til Hometown.

Vidir henviser til social.data.coop hvor snakken kan fortsætte. I øjeblikket skal man skrive på Matrix eller sende 
en mail, hvorefter man kan få et sign up link.

Sam spørger om man kan sætte det op med ansøgning med éns medlems ID, Vidir svarer at vi ikke helt 
har infrastrukturen på nuværende tidspunkt.

Vidir siger at vi forhåbentlig har styr på vores medlemssystem indenfor det næste år og at det fede kunne være
at man kunne oprette brugere på de forskellige services.

Sam advokerer for at vi har et bruger ID, som kan bruges som identifikation, i stedet for email.

## Fremlæggelse af regnskab, budget og kontingent
Vi går over til regnskab. Balder fortæller.

Der er en del gebyrer.

Der står der er 14 betalende kontingenter for 2021, men de kan godt være betalt ind i 2022, så der er i virkeligheden ca. 20 der har betalt for 2021.

Regnskabet af afstemt (går i nul).

Sam spørger om regnskab indeholder drift af server. Balder fortæller at vi ikke bliver faktureret for strøm, da vores leverandør er lidt for god af sig :)

Vi budgetterer for det og vi regner med at begynde at betale på et tidspunkt. Det er ca. 5000 DKK om året.

Vidir spørger til om regnskabet kan godkendes. Det er godkendt.

Vi snakker lidt om hvilke banker det er muligt at skifte til.

Vi kigger på budget for 2022 - Balder fortæller at det er baseret på 2021 budgettet. Der er to ting der er ændret.
Vi har købt strømforsyning.
Domænefornyelse snakker vi om at købe for en del år ad gangen.

Der bliver spurgt ind til hvorfor det er så dyrt med et .coop domæne. Det skyldes bla. at der er en 
manuel oprettelse, hvor man skal verificeres som kooperativ.

Det bliver fremlagt at vi køber domænenavn i tre år ad gangen.

Vidir spørger til om der er nogle indvendinger for budgettet for 2022.
Reynir spørger til at vi har haft en snak om at købe mere HDD plads. Men mener det godt kan vente.
Det bliver nævnt at vi har ca. 900 GB ledig plads pt. Men det kan vente til næste år.

Der bliver spurgt til om vi skal ændre på kontigent. Det bliver ikke ændret.

## Indkomne forslag

Så er vi kommet frem til forslag. Der er to indkommende forslag.
Mikkel foreslår at fremlægge spørgsmålene. 

Mikkel ønsker at præcisere formålsparagraffen med en udvidelse af hvem vi tilbyder vores tjenester til.
Mikkel læser ændringerne op.

## Godkendelse af vedtægtsændringer og Acceptable Use Policy

PR #17 "Forslag til vedtægtsændringer: Ændring og udvidelse af formålsparagraffen" er merget ind i vores vedtægter (dvs. godkendt): https://git.data.coop/data.coop/dokumenter/pulls/17

PR #16 "Forslag til vedtægtsændringer: Tilføjelse til paragraffen om administratorer" er vedtaget via poll (dvs. godkendt). Det er merget ind i vores vedtægter: https://git.data.coop/data.coop/dokumenter/pulls/16

## Valg

Så er vi nået frem til valg til bestyrelsen:

* Reynir stiller op igen. Og er valgt.
* Jesper stiller op igen. Og er valgt.
* Vidir stiller op igen. Og er valgt.

Thomas og Karsten er suppleanter. Thomas stiller sin plads til rådighed.

* Karsten genopstiller og er valgt som suppleant.
* Rasmus melder sig og er hermed valgt som suppleant.

Balder stiller op som revisor igen og er valgt som revisor.

Vidir meddeler at alle poster er besat. Undtagen revisor suppleant, som vi ikke har gjort brug af alligevel.

## Evt.

Vi er nået frem til eventuelt.

Jesper siger han har fundet en stak HDD der er blevet hevet ud af en anden server. De skal ha nogle
caddies og kan installeres i serveren. De skal købes.

Balder fortæller at vi har en velkommen til data.coop på en pad. Den er her: 
https://pad.data.coop/HSVQXoERSVS8W49cuI06qA

Balder opfordrer til at nye medlemmer logger på Matrix og introducerer sig. At det er muligt at spørge
om en introduktion. 
Generel introduktion til foreningen, er det muligt at snakke med Mikkel og Balder.

Når det kommer til introduktion til services/teknik kan man snakke med Vidir, Reynir, Jesper eller Halfdan.

Men der burde være en bedre introduktion til hvordan man sætter sine services op.

Sam spørger til hvor mange medlemmer der er i data.coop. Balder svarer at der pt. er 21 betalende medlemmer.

Der bliver stillet forslag om at lave en guidet tur i vores infrastruktur - behind the scenes. Det ser Vidir på 
at arrangere.

Klaus nævner som en ting til næste gang at ordstyrer siger når vi er skudt igang, så man ved om mikrofon osv. kører.

Over and out.

Vidir siger at der officielt er lukket ned for generelforsamling 2022. Tak for fremmøde og tålmodighed.
